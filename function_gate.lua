mraa = require('mraa')
os = require('os')

Gate_GPIO = 0
Sensor_GPIO = 0
button_GPIO = 0
gate = mraa.Gpio(Gate_GPIO)
gate:dir(mraa.DIR_OUT)
gateState = false
gate:write(0)

sensor = mraa.Gpio(Sensor_GPIO)
sensor:dir(mraa.DIR_OUT)
sensorState = false
sensor:write(0)


button = mraa.Gpio(button_GPIO)
button:dir(mraa.DIR_OUT)
buttonState = false
button:write(0)

local function my_sleep(sec)
   os.execute('sleep ' .. tonumber(sec))
end

while (true) do
   if (gateState == false) then
 	button:write(1)
	gate:write(1)
	sensor:write(0)
	gateState = true
	buttonState = true
	sensorState = false
	my_sleep(1)
	print(gate:read())
   else
	button:write(0)
	gate:write(0)
	sensor:write(1)
	gateState = false
	buttonState = false
	sensorState = true
	my_sleep(1)
	print(gate:read())
   end
end

